import java.util.Scanner;

/**
 * Se citește un nr n de la tastatură. Utilizatorul introduce apoi n nr. afișați pentru fiecare dacă se divide cu 3
 * <p>
 * <p>
 * 4
 * 5
 * nu se divide cu 3
 * 6
 * se divide cu 3
 * 11
 * nu se divide cu 3
 * 33
 * se divide cu 3
 */

/* 0. Început
        1. Citește nr N de la tastatură
        2. De N ori repetă
        Citește nr a de la tastatură
        Dacă a se divide cu 3 afișează "Se divide cu 3"
        Dacă a nu se divide cu 3 afișează "Nu se divide cu 3"
        3. Gata

 */


public class Ex1 {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Introduceti cate numere doriti ");
        int numar = 0;
        int numar1 = 0;
        int divizor = 0;
        numar = keyboard.nextInt();

        System.out.println("Scrieti cu cine vreti sa verificati daca se divid");

        divizor = keyboard.nextInt();

        for (int a = 1; a <= numar; a++) {
            System.out.println("Dati un nr: ");
            numar1 = keyboard.nextInt();
            if (numar1 % divizor == 0) {
                System.out.println("Numarul se divide cu " + divizor);
            } else {
                System.out.println("Numarul nu se divide cu " + divizor);

            }
        }
    }
}