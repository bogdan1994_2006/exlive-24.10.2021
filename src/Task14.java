import java.util.Scanner;

public class Task14 {

    /**
     *
     * Salut, eu sunt Iulia!
     * ["Salut,", "eu", "sunt", "Iulia!"]
     * []
     * ["Salut,","Salut,", "eu", "eu", "sunt", "sunt", "Iulia!", "Iulia!"]
     *
     *
     * 1. Declarăm variabile
     * 2. Citim textul de la tastatură
     * 3. Din textul citit facem un array de cuvinte
     * 4. Din array-ul pe care îl avem vom pune de două ori fiecare cuvânt într-un alt array
     * 5. Afișam ultimul array creat.
     *
     * @param args
     */

    public static void main(String[] args) {
        // declarat variabila keyboard pe care o folosim pentru a citi de la tastatură
        Scanner keyboard = new Scanner(System.in);
        // declarat variabila normal în care vom ține string-ul nebâlbâit
        String normal = "";
        // declarat array-ul pentru string nebâlbâit
        String[] arrayNormal;
        String bâlbâit = "";

        System.out.println("Introduceți textul");
        // am citit de la tastatură string-ul și l-am pus în variabila normal
        normal = keyboard.nextLine();

        arrayNormal = normal.split(" ");
        for (int i = 0; i <= arrayNormal.length - 1; i++) {
            String cuvânt = arrayNormal[i];
            bâlbâit = bâlbâit + cuvânt + " " + cuvânt + " ";
        }
        System.out.println(bâlbâit);
    }
}
